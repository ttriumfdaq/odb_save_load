"""
Client that can save/load bits of the ODB to/from text files on disk.

We store JSON files on disk, and provide a webpage (in custom/saveload.html) that
allows the user to save/load settings as desired.

We allow the user to configure multiple sections of the ODB to be saved. This would
allow the user to save/load detector settings separately from PPG settings, for example.
This configuration is done in /Saveload/Sections. Each entry in that directory is a list
of strings.

The on-disk directory in which files are stored is specified by the ODB key /Saveload/Path.
"""

import midas
import midas.client
import json
import datetime
import os
import os.path
import glob
import collections
import argparse
import ctypes

base_odb_dir = "/Saveload"
latest_api_ver = 2

def parse(filename):
    """
    Load the specified file as JSON.
    
    Args:
        
    * filename (str)
    
    Returns:
        `collections.OrderedDict`
    """
    with open(filename) as f:
        return json.load(f, object_pairs_hook=collections.OrderedDict)

def get_section_disk_dir(section, client):
    """
    Get where on disk a section's saved files will be found.
    
    Args:
        
    * section (str)
    * client (`midas.client.MidasClient`)
    """
    return os.path.join(client.odb_get(base_odb_dir + "/Path"), section)

def create_filename(section):
    """
    Create a new filename for saving settings. We encode the data/time to
    guarantee uniqueness.
    
    Args:
        
    * section (str)
    
    Returns:
        str - Just the filename, not the full path.
    """
    now = datetime.datetime.now()
    return section + "__" + datetime.datetime.strftime(now, "%Y_%m_%d-%H%M%S.json")

def get_file_time(filename):
    """
    Get the time when settings were saved.
    We don't just use the file's modification date as we may have had to tweak
    the settings to adapt to new changes in ODB structure. The user cares about
    when they saved the settings instead.
    
    Args:
        
    * filename (str)
    
    Returns:
        `datetime.datetime`
    """
    datestr = filename.split("__")[-1].replace(".json", "")
    
    try:
        return datetime.datetime.strptime(datestr, "%Y_%m_%d-%H%M%S")
    except:
        return datetime.datetime.fromtimestamp(os.path.getmtime(filename))

def get_live_odb_bits(odb_dir_list, client, for_save=False, to_ctypes=True, match_api_ver=latest_api_ver):
    retval = {}

    for odb_dir in odb_dir_list:
        with_meta = client.odb_get(odb_dir, include_key_metadata=(match_api_ver > 1))
        
        if for_save:
            retval[odb_dir] = with_meta
        else:
            retval[odb_dir] = fix_datatypes(with_meta, odb_dir, client, to_ctypes, match_api_ver)

    return retval

def save(filename, comment, odb_dir_list, client):
    """
    Save the current ODB content to disk.
    
    Args:
        
    * filename (str) - Where to save (full path).
    * comment (str) - Comment to add to the file.
    * odb_dir_list (list of str) - Which bits of the ODB to save. Empty 
        strings are ignored.
    * client (`midas.client.MidasClient`)
    """
    data = {"comment": comment,
            "api_ver": latest_api_ver,
            "odb": get_live_odb_bits(odb_dir_list, client, for_save=True)}
    
    pardir = os.path.dirname(filename)
    
    if not os.path.exists(pardir):
        os.mkdir(pardir)
    
    overwrite_json_file(filename, data)

def overwrite_json_file(file_path, data):
    """
    Create a backup of existing file before writing new
    version, in case we get a JSON encoding error.

    Args:

    * file_path (str)
    * data (dict that can be JSON serialized)
    """
    make_backup = os.path.exists(file_path)

    if make_backup:
        # Make the backup file if doing an overwrite
        backup_file_path = file_path + ".bck"
        os.rename(file_path, backup_file_path)

    try:
        # Try to write the new file
        with open(file_path, "w") as f:
            json.dump(data, f)
    except Exception as e:
        if make_backup:
            # Restore backup version
            print("Failed to write %s. Restoring backup then re-raising the exception.")
            os.rename(backup_file_path, file_path)
        else:
            # Delete corrupt file
            print("Failed to write %s. Deleting file then re-raising the exception.")
            os.remove(file_path)
        raise e
    finally:
        # Delete backup version if still present
        if make_backup and os.path.exists(backup_file_path):
            os.remove(backup_file_path)

def edit_comment(filename, comment):
    """
    Edit the comment metadata in a saved file.
    
    Args:
        
    * filename (str) - File to edit (full path).
    * comment (str) - New comment.
    """
    data = parse(filename)
    data["comment"] = comment
    overwrite_json_file(filename, data)

def force_floats(odb, odb_path, client, to_ctypes=True):
    """
    Force certain ODB keys to be returned as floats rather than ints
    and unsigned ints rather than hex strings.

    Only used for older files that were saved without metadata in them.

    Args:

    * odb (dict) - Part of the ODB, without metadata entries
    * odb_path (str) - Current location in the ODB
    * client (`midas.client.MidasClient`) - Used to look up the true
        type of a hex string (UINT32 or UINT16 etc) if the key already
        exists in the ODB
    * to_ctypes (bool) - Whether to convert values to ctypes (if loading
        into midas) or leave as JSON-serializable types.

    Returns:
        dict
    """
    names = ["time offset (ms)", "pulse width (ms)"]
    
    for k, v in odb.items():
        if isinstance(v, dict):
            odb[k] = force_floats(v, odb_path + "/" + k, client, to_ctypes)
        elif k in names:
            odb[k] = float(v)
        elif isinstance(v, str) and v.startswith("0x"):
            if to_ctypes:
                try:
                    midas_type = client._odb_get_key(odb_path + "/" + k).type
                    odb[k] = client._midas_type_to_ctype(midas_type, initial_value=int(v, 16))
                except:
                    odb[k] = ctypes.c_uint32(int(v, 16))
            else:
                odb[k] = int(v, 16)
        elif isinstance(v, list) and len(v) > 0 and isinstance(v[0], str) and v[0].startswith("0x"):
            if to_ctypes:
                try:
                    midas_type = client._odb_get_key(odb_path + "/" + k).type
                    odb[k] = [client._midas_type_to_ctype(midas_type, initial_value=int(el, 16)) for el in v]
                except:
                    odb[k] = [ctypes.c_uint32(int(el, 16)) for el in v]
            else:
                odb[k] = [int(el, 16) for el in v]
            
    return odb

def reset_debug_params(new_content, from_content):
    """
    Find any ODB keys called "Debug" (recursively) and set them to the
    value in from_content.
    
    Args:
        
    * new_content (dict) - The new values we're about to apply to the ODB.
    * from_content (dict) - The vaues we're about to replace in the ODB, but
        which we want to extract the "Debug" values from.
    """
    if from_content is None:
        return new_content
    
    for k, v in new_content.items():
        if k in from_content:
            if isinstance(v, dict):
                new_content[k] = reset_debug_params(new_content[k], from_content[k])
            elif k == "Debug":
                new_content[k] = from_content[k]
                
    return new_content

def load(filename, odb_dir_list, client, ignore_debug_params=True):
    """
    Load ODB settings from a file into the live ODB.
    
    Args:
        
    * filename (str) - File to load (full path).
    * odb_dir_list (list of str) - Bits of the ODB to load.
    * client (`midas.client.MidasClient`)
    * ignore_debug_params (bool) - Whether to keep the current value of any ODB
        keys named "Debug".
    """
    odb_bits = get_odb_bits(filename, odb_dir_list, client)
    
    original_content = {}
    
    for odb_dir, content in odb_bits.items():
        print("Updating %s" % odb_dir)
        
        try:
            original_content[odb_dir] = client.odb_get(odb_dir)
        except KeyError:
            original_content[odb_dir] = None
        
        if ignore_debug_params:
            content = reset_debug_params(content, original_content[odb_dir])
        
        try:
            client.odb_set(odb_dir, content)
        except midas.MidasError as e:
            # Undo the changes we've made so expt is still in a sane state.
            # (Try to make the loading as atomic as possible).
            for k, v in original_content.items():
                if v is None:
                    client.odb_delete(k)
                else:
                    client.odb_set(k, v)
            
            # Now raise the error.
            if e.code == midas.status_codes["DB_OPEN_RECORD"]:
                raise midas.MidasError(e.code, "Structure of '%s' has changed since settings were saved, and a client has an open record on it so we can't fix it" % odb_dir)
            else:
                raise

def apply_tids_and_strip_metadata(obj, client):
    """
    Convert from python types that are JSON serializable (but
    ambiguous) to ctypes that can be loaded into the ODB. Most
    important for things like UINT32 vs UINT16.

    Assumes that metadata is included in the dict (so we can
    look up the type). Metadata entries will be stripped out.

    E.g.
    `{"some_name": 1, "some_name/key": {"type": 4}}` will become
    `{"some_name": ctypes.c_uint16(1)}`, as 4 means midas.TID_UINT16.

    Args:
        
    * obj (dict)
    * client (`midas.client.MidasClient`)

    Returns:
        Manipulated copy of `obj` 
    """
    if not isinstance(obj, dict):
        return obj

    # Initialize as an ordered dict, dict or whatever
    retval = type(obj)()

    for k,v in obj.items():
        if k.endswith("/key"):
            continue
        if isinstance(v, dict):
            retval[k] = apply_tids_and_strip_metadata(v, client)
        elif k+"/key" in obj:
            tid = obj[k+"/key"]["type"]

            if tid == midas.TID_STRING:
                # Avoid a lot of nastiness converting to cstring and back.
                # Just pass the string / list of strings
                retval[k] = v
            elif tid == midas.TID_BOOL:
                retval[k] = v
            else:
                # Convert to UINT32/INT32/UINT16 etc...
                retval[k] = client._midas_type_to_ctype(tid, initial_value=v)
        else:
            retval[k] = v

    return retval

def fix_datatypes(obj, odb_dir, client, to_ctypes, api_ver):
    """
    Wrapper function for correcting the data types that are ambiguous in
    JSON dumps.

    * Files with API version >= 2 include metadata dictating the type, so we
        have no issues.
    * Files with API version == 1 (or not API version) do not have metadata, so
        we make educated guesses about converting to floats/unsigned.

    Args:

    * obj (dict)
    * odb_path (str) - Current location in the ODB
    * client (`midas.client.MidasClient`)
    * to_ctypes (bool) - Whether to convert values to ctypes (if loading
        into midas) or leave as JSON-serializable types.
    * api_ver (int) - API version that `obj` was saved/read with

    Returns:
        Manipulated copy of `obj` 
    """
    if api_ver > 1:
        # Newer files store the metadata.
        top_key = odb_dir.rstrip("/").split("/")[-1]
        if to_ctypes:
            return apply_tids_and_strip_metadata(obj, client)[top_key]
        else:
            return client._prune_metadata_from_odb(obj)[top_key]
    else:
        # Older files don't store the metadata. Try to magically 
        # deduce the correct types.
        return force_floats(obj, odb_dir, client, to_ctypes)

def get_odb_bits(filename, odb_dir_list, client, raise_if_not_present=True, to_ctypes=True, return_api_ver=False):
    """
    Get the specified parts of the ODB from a file. If a required
    bit is missing, we raise an error. This slightly helps prevent
    cases of loading very old saved files where the ODB structure is
    now very different.
    
    Args:
        
    * filename (str) - File to load (full path).
    * odb_dir_list (list of str) - Bits of the ODB to load.
    * raise_if_not_present (bool) - Raise an exception if an ODB dir isn't in 
        the saved file. If False, return None for that entry.
    * to_ctypes (bool) - Whether to convert values to ctypes (if loading
        into midas) or leave as JSON-serializable types.
    * return_api_ver (bool) - Whether to include the API version of the file
        in the return value.
    
    Returns:
        If `return_api_ver` is False:
            dict of {str: `collections.OrderedDict`}, keyed by ODB path.
        If `return_api_ver` is True:
            2-tuple of (bool, dict) for (API version, ODB bits)
    """
    js_data = parse(filename)
    retval = {}
    api_ver = js_data["api_ver"] if "api_ver" in js_data else 1
    
    for odb_dir in odb_dir_list:
        if odb_dir == "":
            continue
        
        if odb_dir in js_data["odb"]:
            retval[odb_dir] = fix_datatypes(js_data["odb"][odb_dir], odb_dir, client, to_ctypes, api_ver)
        elif raise_if_not_present:
            raise ValueError("ODB directory '%s' is not present in the saved file! You may want to use the save_load.py script to add it." % odb_dir)
        else:
            retval[odb_dir] = None
    
    if return_api_ver:
        return (retval, api_ver)
    else:
        return retval

def filelist(disk_dir):
    """
    Get the list of JSON files in the specified directory.
    
    Args:
        
    * disk_dir (str)
    
    Returns:
        list of dict. One dict per file, ordered by most recently-edited file.
        Dict keys are filename, comment, and last_modified.
    """
    files = []
    
    if os.path.exists(disk_dir):
        filenames = glob.glob(disk_dir + "/*.json")
        
        for filename in filenames:
            js_data = parse(filename)
            meta = {"filename": filename, 
                    "comment": js_data["comment"], 
                    "api_ver": js_data["api_ver"] if "api_ver" in js_data else 1, 
                    "last_modified": str(get_file_time(filename))}
            files.append(meta)
    
    return sorted(files, key=lambda x: x["last_modified"], reverse=True)

def diff_json(a, b, a_name, b_name, curr_path=""):
    """
    Recursively diff two JSON objects.

    Args:

    * a (dict) - Object to compare
    * b (dict) - Object to compare
    * a_name (str) - Prefix to use when displaying a value from `a`
    * b_name (str) - Prefix to use when displaying a value from `b`
    * curr_path (str) - Current location in the ODB/dicts

    Returns:
        4-tuple of (list, list, dict, dict) for 
        ("keys only in a", "keys only in b", "objects with different values",
         "objects with different types").
        Keys in the final dicts are ODB paths. Values are a dict with keys
        based on `a_name` and `b_name`.

        E.g. the third element of the tuple could look like:
        `{"/Path/to/setting": {"Saved value": 1, "Live value": 2}}` if
        `a_name` was "Saved value" and `b_name` was "Live value".
    """
    only_a = []
    only_b = []
    diff_vals = {}
    diff_types = {}

    for k, v_a in a.items():
        this_path = k if curr_path == "" else "%s/%s" % (curr_path, k)

        if k not in b:
            only_a.append(this_path)
        else:
            v_b = b[k]

            if type(v_a) != type(v_b):
                diff_types[this_path] = collections.OrderedDict([(a_name, v_a), (b_name, v_b)])
            elif isinstance(v_a, dict):
                (sub_a, sub_b, sub_dv, sub_dt) = diff_json(v_a, v_b, a_name, b_name, this_path)
                only_a.extend(sub_a)
                only_b.extend(sub_b)
                diff_vals.update(sub_dv)
                diff_types.update(sub_dt)
            elif v_a != v_b:
                diff_vals[this_path] = collections.OrderedDict([(a_name, v_a), (b_name, v_b)])

    for k, v_b in b.items():
        this_path = k if curr_path == "" else "%s/%s" % (curr_path, k)

        if k not in a:
            only_b.append(this_path)

    return (only_a, only_b, diff_vals, diff_types)

def rpc_handler(client, cmd, args, max_len):
    """
    JRPC handler we register with midas. This will generally be called when
    a user clicks a button on the associated webpage.
    
    Args:
        
    * client (`midas.client.MidasClient`)
    * cmd (str) - The command the user wants us to do.
    * args (str) - Stringified JSON of any arguments for this command.
    * max_len (int) - Maximum length of response the user will accept.
    
    Returns:
        2-tuple of (int, str) for (status code, message)
        
    Accepted commands:
        
    * save - Save live ODB to disk
    * edit_comment - Edit the comment in a file
    * delete - Delete settings from disk
    * load - Load settings from disk into live ODB
    * view - Return the content of a file
    * diff - Return the differences between the live ODB and a file
    * list - List the files available
    """
    jargs = json.loads(args)
    section = jargs["section"]
    settings = client.odb_get(base_odb_dir)
    
    if section not in settings["Sections"]:
        raise ValueError("Unknown section '%s' - configure it in ODB at %s/Sections/%s" % (section, base_odb_dir, section))
    
    print("Handling RPC function %s" % cmd)
    base_disk_dir = get_section_disk_dir(section, client)
    retstr = ""
    
    if cmd == "save":
        comment = jargs["comment"]
        odb_dir_list = [d for d in settings["Sections"][section] if d != ""]
        filename = os.path.join(base_disk_dir, jargs.get("filename", create_filename(section)))
        save(filename, comment, odb_dir_list, client)
    elif cmd == "edit_comment":
        comment = jargs["comment"]
        filename = os.path.join(base_disk_dir, jargs["filename"])
        edit_comment(filename, comment)
    elif cmd == "delete":
        filename = os.path.join(base_disk_dir, jargs["filename"])
        os.unlink(filename)
    elif cmd == "load":
        filename = os.path.join(base_disk_dir, jargs["filename"])
        odb_dir_list = [d for d in settings["Sections"][section] if d != ""]
        load(filename, odb_dir_list, client)
        retstr = "Loaded successfully"
    elif cmd == "view":
        filename = os.path.join(base_disk_dir, jargs["filename"])
        odb_dir_list = [d for d in settings["Sections"][section] if d != ""]
        odb = get_odb_bits(filename, odb_dir_list, client, to_ctypes=False)
        retstr = json.dumps(odb, indent=2)
    elif cmd == "diff":
        filename = os.path.join(base_disk_dir, jargs["filename"])
        odb_dir_list = [d for d in settings["Sections"][section] if d != ""]
        (file_odb, api_ver) = get_odb_bits(filename, odb_dir_list, client, to_ctypes=False, return_api_ver=True)
        live_odb = get_live_odb_bits(odb_dir_list, client, to_ctypes=False, match_api_ver=api_ver)
        (only_a, only_b, diff_vals, diff_types) = diff_json(file_odb, live_odb, "Saved value", "Live value")
        json_diff = collections.OrderedDict()
        if len(only_a):
            json_diff["Keys that are only in saved settings"] = sorted(only_a)
        if len(only_b):
            json_diff["Keys that are only in live ODB"] = sorted(only_b)
        if len(diff_types):
            ordered_keys = sorted(diff_types.keys())
            retdiff = collections.OrderedDict()
            for k in ordered_keys:
                retdiff[k] = diff_types[k]
            json_diff["Types that differ between saved settings and live ODB"] = retdiff
        if len(diff_vals):
            ordered_keys = sorted(diff_vals.keys())
            retdiff = collections.OrderedDict()
            for k in ordered_keys:
                retdiff[k] = diff_vals[k]
            json_diff["Values that differ between saved settings and live ODB"] = retdiff
        if len(json_diff):
            retstr = json.dumps(json_diff, indent=2)
        else:
            retstr = "No differences found"
    elif cmd == "list":
        retstr = json.dumps(filelist(base_disk_dir))
    else:
        raise ValueError("Unknown command '%s'" % cmd)
    
    if len(retstr) > max_len:
        raise ValueError("Return string too long (want %s chars but buffer is size %s)" % (len(retstr), max_len))
    
    return (midas.status_codes["SUCCESS"], retstr)

def find_section_for_path(find_path, client):
    """
    Helper function to find which "section" a given ODB path is saved in.
    
    Args:
        
    * find_path (str) - The ODB path the find
    * client (midas.client.MidasClient)
    
    Returns:
        2-tuple of (str, list of str) for the section name and list of
            ODB paths saved in this section
    """
    sections = client.odb_get(base_odb_dir + "/Sections")
    
    for section, path_list in sections.items():
        for path in path_list:
            if find_path.startswith(path):
                return (section, path_list)
            
    return (None, None)

def recurse_add_delete(action, edit_odb_path, odb_dir, content, filename, odb_value, odb_dir_index, client, api_ver=1):
    """
    Helper function for recursing through a saved file, to add or delete the setting
    of interest.
    
    Args:
        
    * action (str) - "add", "delete" or "edit"
    * edit_odb_path (str) - Full ODB path the add/delete from files
    * odb_dir (str) - Current location in the saved file
    * content (str) - Content of odb_dir in the saved file
    * filename (str) - The file we're currently editing
    * odb_value (dict or None) - If adding a new entry, the data to add. 
        Keys should be of form "odb_name" and "odb_name/key"
    * odb_dir_index (int or None) - If action is `add` or `add_from_live`, and the
        order of the new key in the parent directory is important, this should be
        the new position. If None, it'll be added at the end.
    * api_ver (int) - API version of the file. If >= 2 need to edit metadata
    
    Returns:
        dict - The new data
    """
    if not edit_odb_path.startswith(odb_dir):
        return content

    searching = edit_odb_path.replace(odb_dir, "")
    
    if searching.startswith("/"):
        searching = searching[1:]
    if searching.endswith("/"):
        searching = searching[:-1]
        
    bits = searching.split("/")

    if len(bits) == 1:
        # Bottom level - add or delete
        if action == "delete":
            if searching == "" or content is None:
                # Top-level entry
                if content is None:
                    print("%s - did not contain section '%s'" % (filename, edit_odb_path))
                else:
                    print("%s - deleting entire section '%s'" % (filename, edit_odb_path))
                return None
            elif searching in content.keys():
                print("%s - deleting '%s'" % (filename, edit_odb_path))
                del content[searching]
            else:
                print("%s - did not contain '%s'" % (filename, edit_odb_path))
            
        if action == "add" or action == "edit":
            if searching == "" and content is None:
                # Top-level entry
                print("%s - %sing entire section '%s'" % (filename, action, edit_odb_path))
                content = odb_value
            elif isinstance(content, dict):
                if searching in content.keys() and action != "edit":
                    print("%s - already contained '%s'" % (filename, edit_odb_path))
                else:
                    print("%s - %sing '%s'" % (filename, action, edit_odb_path))
                    content = add_value_to_dict(content, searching, odb_value, odb_dir_index, api_ver, client)

            elif content is None:
                print("%s - '%s' is not present in the file, so can't add sub-keys. Add the full dir first." % (filename, odb_dir))
            else:
                print("%s - '%s' is not a dict, so can't add key %s" % (filename, odb_dir, searching))
                
    elif isinstance(content, dict) and bits[0] in content.keys():
        # Recurse down
        new_path = odb_dir + "/" + bits[0]
        content[bits[0]] = recurse_add_delete(action, edit_odb_path, new_path, content[bits[0]], filename, odb_value, odb_dir_index, client, api_ver)
    elif action == "add" or action == "edit":
        # Need to create sub-dirs
        print("%s - adding '%s' and parent dirs" % (filename, edit_odb_path))
        k = bits[-1]
        data = {k: odb_value[k]}

        if api_ver > 1:
            data[k + "/key"] = odb_value[k + "/key"]
        
        for i in range(len(bits) - 2, 0, -1):
            data = {bits[i]: data}
            
        if content is None:
            content = collections.OrderedDict()

        content[bits[0]] = data
    
    return content            

def add_value_to_dict(content, odb_key, odb_value, odb_dir_index, api_ver, client):
    if odb_dir_index is None:
        retval = content

        if api_ver > 1:
            retval[odb_key] = odb_value[odb_key]
            retval[odb_key + "/key"] = odb_value[odb_key + "/key"]
        else:
            retval[odb_key] = client._prune_metadata_from_odb(odb_value[odb_key])
    else:
        retval = collections.OrderedDict()
        # Account for the fact that we have xxx and xxx/key entries in api_ver 2
        new_item_pos = odb_dir_index if api_ver == 1 else odb_dir_index * 2

        for idx, (key, value) in enumerate(content.items()):
            if idx == new_item_pos:
                # Add new item
                if api_ver > 1:
                    retval[odb_key] = odb_value[odb_key]
                    retval[odb_key + "/key"] = odb_value[odb_key + "/key"]
                else:
                    retval[odb_key] = client._prune_metadata_from_odb(odb_value[odb_key])

            # Copy existing item
            retval[key] = value

    return retval

def edit_saved(action, edit_odb_path, json_value=None, client=None, odb_dir_index=None):
    """
    Edit saved settings on disk, by adding or deleting a value from all files.
    
    Args:
        
    * action (str) - "add", "add_from_live", "delete", "edit", "edit_from_live"
    * edit_odb_path (str) - Full ODB path the add/delete from files
    * json_value (str) - If adding a new entry, the JSON-encoded data. The actual
        data added should be beneath a top-level key called "value".
        E.g. '{"value": true}' will result in True being added.
             '{"value": {"subkey": "subval"}}' will add {"subkey": "subval"}.
    * client (`midas.client.MidasClient`)
    * odb_dir_index (int or None) - If action is `add` or `add_from_live`, and the
        order of the new key in the parent directory is important, this should be
        the new position. If None, it'll be added at the end.
    """
    if client is None:
        client = midas.client.MidasClient("saveload_editor")

    (section, odb_dir_list) = find_section_for_path(edit_odb_path, client)
    
    if section is None:
        raise ValueError("Don't know where path should be saved")
    
    if action == "add_from_live" or action == "edit_from_live":
        odb_value = client.odb_get(edit_odb_path, include_key_metadata=True)
        action = action.replace("_from_live", "")

        # Workaround edge case handling bools in midas python code
        kn = edit_odb_path.split("/")[-1]
        tid = odb_value[kn + "/key"]["type"]

        if tid == midas.TID_BOOL:
            odb_value[kn] = True if odb_value[kn] else False
    elif action == "add" or action == "edit":
        jargs = json.loads(json_value)
        topkey = edit_odb_path.split("/")[-1]
        v = jargs["value"]

        if "type" not in jargs:
            jargs["type"] = client._ctype_to_midas_type(v)
            
        odb_value = {topkey: v, topkey+"/key": {"type": jargs["type"]}}
    else:
        odb_value = None
        
    base_dir = get_section_disk_dir(section, client)
    print("Finding saved files in %s" % base_dir)
    
    files = filelist(base_dir)
    
    for file in files:
        filename = file["filename"]
        api_ver = file["api_ver"]
        file_path = os.path.join(base_dir, filename)
        
        odb_bits = parse(filename)["odb"]
        
        for odb_dir, content in odb_bits.items():
            if api_ver == 1:
                # Content structured like:
                # v1: {"/Path/to/section": data}
                odb_bits[odb_dir] = recurse_add_delete(action, edit_odb_path, odb_dir, content, filename, odb_value, odb_dir_index, client, api_ver)
            else:
                # Content structured like:
                # v2: {"/Path/to/section": {"section": data}}
                topkey = odb_dir.rstrip("/").split("/")[-1]
                odb_bits[odb_dir][topkey] = recurse_add_delete(action, edit_odb_path, odb_dir, content[topkey], filename, odb_value, odb_dir_index, client, api_ver)
 
        odb_bits = {k:v for k,v in odb_bits.items() if v is not None}

        data = {"comment": file["comment"],
                "api_ver": api_ver,
                "odb": odb_bits}
        
        overwrite_json_file(file_path, data)

def add_from_live_if_section_exists(client, odb_path, odb_dir_idx=None, dry_run=False):
    """
    Edit saved settings on disk, if the specified path exists in the ODB and is
    configured for one of the sections. Helpful for automating ODB schema migrations
    so you can run the same script for every experiment, even if they're not using
    all the same features. E.g. it's safe to call this function for
    ODB path `/Scanning/Epics/Settings/X use formula` even if an experiment isn't
    using EPICS scanning (as no change will be made as the ODB key won't exist).

    Args:
        
    * client (`midas.client.MidasClient`)
    * odb_path (str) - Full ODB path the add/delete from files
    * odb_dir_index (int or None) - If action is `add` or `add_from_live`, and the
        order of the new key in the parent directory is important, this should be
        the new position. If None, it'll be added at the end.
    * dry_run (bool) - If True, just print what would happen
    """
    if not client.odb_exists(odb_path):
        print("Skipping %s as not present in ODB" % odb_path)
        return

    if find_section_for_path(odb_path, client)[0] is None:
        print("Skipping %s as not in any section" % odb_path)
  
    if dry_run:
        print("Would edit %s" % odb_path)
    else:
        edit_saved("add_from_live", odb_path, odb_dir_index=odb_dir_idx, client=client)

def main():
    """
    Main function that starts a midas client and runs forever.
    """
    client = midas.client.MidasClient("saveload")
    
    if not client.odb_exists(base_odb_dir):
        default_settings = {"Path": client.odb_get("/Logger/Data dir"),
                            "Sections": {"PPGScan": ["/Equipment/PPGCompiler/Settings",
                                                     "/Equipment/PPGCompiler/Programming",
                                                     "/Scanning",
                                                     "",
                                                     "",
                                                     "",
                                                     ""]}}
        client.odb_set(base_odb_dir, default_settings)

    client.register_jrpc_callback(rpc_handler, True)
    
    while True:
        client.communicate(1000)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Client that saves/loads ODB settings to/from text files based on RPC requests. No arguments => run client forever.")
        
    subparsers = parser.add_subparsers(help='Sub-command help', dest="command")
    parser_run = subparsers.add_parser("run", description='Run the client, waiting for RPC commands (the default)')
    
    parser_add = subparsers.add_parser("add", description='Add an ODB key to saved settings that are missing it')
    parser_add.add_argument("--odb-path", required=True)
    parser_add.add_argument("--json-value", required=True, help="Encode data as '{\"value\": true}' etc (always using \"value\" as the key name")
    parser_add.add_argument("--odb-dir-index", type=int, help="Specify the position of the new key in the parent ODB directory, if that is important")

    parser_adl = subparsers.add_parser("add_from_live", description='Add an ODB key to saved settings that are missing it, using the current value from the ODB')
    parser_adl.add_argument("--odb-path", required=True)
    parser_adl.add_argument("--odb-dir-index", type=int, help="Specify the position of the new key in the parent ODB directory, if that is important")
    
    parser_del = subparsers.add_parser("delete", description='Delete an ODB key from saved settings that contain it')
    parser_del.add_argument("--odb-path", required=True)
    
    parser_edt = subparsers.add_parser("edit", description='Change value of an ODB key in saved settings, or add it to those that are missing it')
    parser_edt.add_argument("--odb-path", required=True)
    parser_edt.add_argument("--json-value", required=True, help="Encode data as '{\"value\": true}' or '{\"value\": true, \"type\": 8}' etc (always using \"value\" as the key name, and optionally specifying a midas TID for an explicit type. See midas.__init__.py for list of types)")

    parser_edl = subparsers.add_parser("edit_from_live", description='Change value of an ODB key in saved settings, or add it to those that are missing it, using the current value from the ODB')
    parser_edl.add_argument("--odb-path", required=True)    
    
    args = parser.parse_args()
    
    if not hasattr(args, "command") or args.command is None or args.command == "run":
        main()
    elif args.command == "add" or args.command == "edit":
        dir_idx = args.odb_dir_index if args.command == "add" else None
        edit_saved(args.command, args.odb_path, args.json_value, odb_dir_index=dir_idx)
    elif args.command == "add_from_live" or args.command == "edit_from_live":
        dir_idx = args.odb_dir_index if args.command == "add_from_live" else None
        edit_saved(args.command, args.odb_path, odb_dir_index=dir_idx)
    elif args.command == "delete":
        edit_saved("delete", args.odb_path)
