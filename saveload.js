var ds = ds || {};

/**
* Functions for the saveload.html page. Interacts with the saveload.py program
* that saves/loads bits of the ODB to/from JSON files on disk. 
*/
ds.saveload = (function() {
  var client_name = "SaveLoad";
  var section_name;
  var done_init = false;

  var encode_entities = function(raw) {
    return raw.replace(/[\u00A0-\u9999<>\&]/g, ((i) => `&#${i.charCodeAt(0)};`));
  }

  /**
  * Report an RPC error as a dialog box. Provides a more human-readable explanation
  * for status 103 (CM_NO_CLIENT).
  *
  * @protected
  * @param {number} status - The return code from the RPC call (a midas status code)
  * @param {string} reply - The error message
  */
  var alert_rpc_error = function(status, reply) {
    if (status == 103) {
      dlgAlert("The save/load frontend must be running for this functionality to work."); 
    } else {
      dlgAlert("Failed to perform action!<div style='text-align:left'><br>Status code: " + status + "<br>Message: " + encode_entities(reply) + "</div>"); 
    }  
  };
  
  /**
  * Utility to parse a response from a JRPC client that encodes its real result
  * as JSON.
  *
  * Normally if a JRPC client returns a status that isn't SUCCESS (aka 1),
  * midas will only return that code and not a message. So some clients return
  * SUCCESS (to ensure a useful error message can be returned) and encode the
  * "real" status in the reply: {"code": real_status_code, "msg": error_message}.
  *
  * This function unpacks the "real" status code and message.
  *
  * @param {Object} - RPC result from calling `mjsonrpc_call()`.
  * @return {Array} - 2 elements - [0] the numeric code, [1] the string message.
  */
  var parse_rpc_response = function(rpc_result) {
    let status = rpc_result.status;
    let reply = "";

    if (status == 1) {
      // Only get a reply from mjsonrpc if status is 1
      let parsed = JSON.parse(rpc_result.reply);
      status = parsed["code"];
      reply = parsed["msg"];
    }

    return [status, reply];
  };
  
  /**
  * Sets a cookie in the user's browser.
  *
  * @param {string} name - Cookie name.
  * @param {string} value - Cookie value.
  * @param {number} days - Cookie validity length.
  */
  var set_cookie = function(name, value, days) {
    let expires = "";

    if (days) {
      let date = new Date();
      date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
      expires = "; expires=" + date.toGMTString();
    }

    document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/; SameSite=Lax";
  };

  /**
  * Gets a cookie from the user's browser.
  *
  * @param {string} name - Cookie name.
  * @returns {?string} - Cookie value.
  */
  var get_cookie = function(name) {
    var name_equals = encodeURIComponent(name) + "=";

    var cookies = document.cookie.split(';');

    for (var i = 0; i < cookies.length; i++) {
      // Strip leading whitespace from cookie name
      var stripped = cookies[i].replace(/^ /g, '');

      if (stripped.indexOf(name_equals) === 0) {
        // Found the cookie
        return decodeURIComponent(stripped.substring(name_equals.length, stripped.length));
      }
    }

    return null;
  };

  /**
  * Deletes a cookie from the user's browser.
  *
  * @param {string} name - Cookie name.
  */
  var delete_cookie = function(name) {
    set_cookie(name, "", -1);
  };
    
  /**
  * Communicates with the saveload client via RPC, and calls the callback on success.
  * Displays an error on failure.
  * 
  * @protected
  * @param {string} cmd - JRPC command
  * @param {Object} args - Arguments for the JRPC command
  * @param {function} callback - Function to call on success. Should accept 1 argument,
  *     the string result from the JRPC call
  * @param {int?} maxlen - Maximum length of response to accept (in chars).
  */
  var saveload_rpc = function(cmd, args, callback, maxlen) {
    let params = Object()
    params.client_name = client_name;
    params.cmd = cmd;
    params.args = JSON.stringify(args);
    
    if (maxlen !== undefined) {
      params.max_reply_length = maxlen;
    }
  
    mjsonrpc_call("jrpc", params).then(function(rpc) {
      let [status, reply] = parse_rpc_response(rpc.result);
      if (status == 1) {
        callback(reply);
      } else {
        alert_rpc_error(status, reply);
      }
    });
  };
  
  /**
  * Entry function that grabs the current list of saved settings from the client,
  * builds a table to show them to the user, then registers this function with
  * mhttpd.
  */
  var init = function() {
    mjsonrpc_db_get_value("/Saveload/Sections").then(function(rpc) {
      let res = rpc.result.data[0];
      
      if (res !== null) {
        for (let s in res) {
          if (s.indexOf("/") == -1) {
            let name = res[s + "/name"];
          
            var option = document.createElement("option");
            option.text = name;
            document.getElementById("section_sel").add(option); 
          }
        }
        
        // Default to showing the settings the user last displayed
        let cookie_section = get_cookie("section");
        
        if (cookie_section === null) {       
          document.getElementById("section_sel").selectedIndex = 0;
        } else {
          document.getElementById("section_sel").value = cookie_section;
        }
        
        document.getElementById("section_sel").onchange();
      }
    });
  };
  
  var section_changed = function(new_section_name) {
    show_settings("");
    section_name = new_section_name;
    let elements = document.getElementsByClassName("human_name");
    
    for (let i in elements) {
      elements[i].innerHTML = section_name;
    }
    
    set_cookie("section", section_name);
      
    let args = {"section": section_name};
    saveload_rpc("list", args, populate_list, 1e6);
    
    if (!done_init) {
      done_init = true;
      mhttpd_init('Save/load settings');
    }
  }
  
  var runstate_changed = function() {
    let running = (document.getElementById("runstate").innerHTML == "3");
    
    document.getElementById("running_warning").style.display = (running ? "block" : "none");
    disable_while_running(running);
  }
  
  /**
  * Disables/enabled any input elements with the "disable_running"
  * class depending on whether the run is in progress or not.
  *
  * @protected
  * @param {boolean} disable - Whether to disable or enable the inputs.
  */
  var disable_while_running = function(disable) {
    let elements = document.getElementsByClassName("disable_running");
    
    if (elements.length == 0) {
      return;
    }
    
    for (let i in elements) {
      // Disabled input
      elements[i].disabled = disable;

      // As well as just disabling inputs, we need special
      // logic for any inputs that are being updated automatically
      // by midas. In particular we must remove the "data-odb-editable"
      // attribute when disabling, and add it back when enabling.
      // We use the "data-was-editable" attribute to keep track of
      // which elements we stripped the attribute from.
      let dataset = elements[i].dataset;
      
      if (disable) {
        if (dataset && "odbEditable" in dataset) {
          dataset["wasEditable"] = "1";
          delete dataset["odbEditable"];
          elements[i].innerHTML = "";
        }        
      } else {
        if (dataset && "wasEditable" in dataset) {
          dataset["odbEditable"] = "1";
          delete dataset["wasEditable"];
          elements[i].innerHTML = "";
        }        
      }
    }
  };
  
  /**
  * Build a table showing all the saved settings that can be loaded.
  * Fills the DOM element with ID "settings_list".
  *
  * @protected
  * @param {string} rpc_msg - Response from the saveload client. Stringified JSON
  *     for a list of dicts. Dicts have keys last_modified, comment, filename.
  */
  var populate_list = function(rpc_msg) {
    let files = JSON.parse(rpc_msg);
    let html = "";
    
    if (files.length) {
      
      for (let f in files) {
        let mod = files[f]["last_modified"];
        if (mod.indexOf(".") != -1) {
          // Strip any millisecs if present
          mod = mod.slice(0, mod.indexOf("."))
        }
        html += '<tr>';
        html += '<td>' + files[f]["comment"] + '</td><td>' + mod + '</td>';
        html += '<td><button role="button" class="mbutton disable_running" onclick="ds.saveload.start_load(\'' + files[f]["filename"]  + '\')">Load these settings</button>';
        html += '<button role="button" class="mbutton" onclick="ds.saveload.start_view(\'' + files[f]["filename"]  + '\')">View settings</button>';
        html += '<button role="button" class="mbutton" onclick="ds.saveload.start_diff(\'' + files[f]["filename"]  + '\')">Diff to live settings</button>';
        html += '<button role="button" class="mbutton" onclick="ds.saveload.start_edit_comment(\'' + files[f]["filename"]  + '\')">Edit comment</button>';
        html += '<button role="button" class="mbutton" onclick="ds.saveload.start_delete(\'' + files[f]["filename"]  + '\')">Delete</button>';
        html += '</td>';
        html += '</tr>';
      }
      
    } else {
      html += "<tr><td colspan='3'>No saved files found.</td></tr>";
    }  
    
    document.getElementById("settings_list").innerHTML = html;
  };
  
  /**
  * Display the ODB content of a set of saved settings to the user.
  * Fills the DOM element with ID "settings".
  *
  * @protected
  * @param {string} rpc_reply - saved settings to display.
  */
  var show_settings = function(rpc_reply) {
    document.getElementById("settings").innerHTML = rpc_reply;
  };
  
  /**
  * Inform the user that saved settings have been loaded.
  * Fills the DOM element with ID "settings".
  *
  * @protected
  * @param {string} rpc_reply - saved settings to display.
  */
  var show_loaded = function(rpc_reply) {
    document.getElementById("settings").innerHTML = "Loading saved settings: " + rpc_reply;
  };
  
  /**
  * Helper function to just refresh the page if a JRPC command
  * complete successfully.
  *
  * @protected
  * @param {string} rpc_reply - response from JRPC command (ignored).
  */
  var refresh = function(rpc_reply) {
    location.reload();
  };
  
  /**
  * Submit a request to save the current ODB settings. 
  * Reads comment from the DOM element with ID "comment".
  */
  var start_save = function() {
    let args = {"section": section_name, "comment": document.getElementById("comment").value};
    saveload_rpc("save", args, refresh); 
  };
  
  /**
  * Submit a request to load a set of ODB settings from disk.
  * 
  * @param {string} filename - file to edit the comment for.
  */
  var start_load = function(filename) {
    let args = {"section": section_name, "filename": filename};
    saveload_rpc("load", args, show_loaded); 
  };
  
  /**
  * Submit a request to view a set of ODB settings on disk.
  *
  * @param {string} filename - file to view.
  */
  var start_view = function(filename) {
    let args = {"section": section_name, "filename": filename};
    saveload_rpc("view", args, show_settings, 1e6); 
  };
  
  /**
  * Submit a request to diff a set of ODB settings on disk against the current settings.
  *
  * @param {string} filename - file to diff against.
  */
  var start_diff = function(filename) {
    let args = {"section": section_name, "filename": filename};
    saveload_rpc("diff", args, show_settings, 1e6); 
  };
  
  /**
  * Submit a request to edit the comment associated with a set of ODB 
  * settings that are already on disk.
  * Prompts the user for new comment.
  *
  * @param {string} filename - file to edit the comment for.
  */
  var start_edit_comment = function(filename) {
    let comment = prompt("Enter new comment:");
    
    if (comment.length == 0) {
      return;
    }
    
    let args = {"section": section_name, "filename": filename, "comment": comment};
    saveload_rpc("edit_comment", args, refresh); 
  };
  
  /**
  * Confirm that the user really wants to delete a set of saved ODB
  * settings from disk, then submit the request.
  * 
  * @param {string} filename - file to edit the comment for.
  */
  var start_delete = function(filename) {
    if (!confirm("Are you sure you want to delete these saved settings from disk?")) {
      return;
    }
    
    let args = {"section": section_name, "filename": filename};
    saveload_rpc("delete", args, refresh); 
  };
  
  return {
    init: init,
    start_delete: start_delete,
    start_edit_comment: start_edit_comment,
    start_load: start_load,
    start_save: start_save,
    start_view: start_view,
    start_diff: start_diff,
    section_changed: section_changed,
    runstate_changed: runstate_changed
  };
})();