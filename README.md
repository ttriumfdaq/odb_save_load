# Tools for saving/loading ODB settings

This repository contains tools for saving/loading parts of a midas ODB, using a python script or a webpage.

## Overview

The general concept is to keep the python script running all the time, and it will respond to actions the user performs on the webpage. The user can view the list of saved settings, request settings be saved, load the settings, delete settings etc. The saved settings themselves are stored as JSON files on disk.

The tools allow you to specify which bits of the ODB get saved. You can also specify multiple "sections", each of which can contain multiple ODB directories. For example, you may want to save some "calibration" parameters independently from some "digitizer" parameters. The "calibration" section would save/load `/Equipment/Calib/Settings`, while the "digitizer" section could save/load `/Equipment/Dig1/Settings` and `/Equipment/Dig2/Settings`.

The configuration of "sections" and the corresponding ODB keys to save are themselves stored in the OBD.

## ODB settings

When you first run the python script, it will create the `/Saveload` directory in the ODB.

* `/Saveload/Path` is a string stating where to store the JSON files. The actual files are written in sub-directories of here, as `<user_path>/<section_name>/<file_name>.json`.
* `/Saveload/Sections` is a directory that will contain string arrays. Each array name is the name of a section. The values are the bits of the ODB to save as part of that section. The arrays can be as long as you want.

## The python script

`save_load.py` is a midas client. By default it will set up an RPC server, and listen for commands from the web page.

The script requires python 3.6 or higher. You must have the [midas python tools](https://midas.triumf.ca/MidasWiki/index.php/Python) available.

### Manipulating the saved settings

Experiments often edit their DAQ code to add/remove keys. This can cause a problem if you later try to load some settings that were saved with the "old" ODB structure. Depending on how your DAQ code is written, midas may prevent you from loading the new settings (if a client has an open record / hotlink on the structure you want to change). Or you may just end up with a different ODB structure to what you / your code expected.

To avoid these issues, the python script allows you to adjust the saved settings in a fairly user-friendly way.

All of the commands accept an `--odb-path` argument. The script will automatically determine which section that ODB path belongs to, find the right bit of the ODB structure in the saved file, and modify it accordingly.

The possible commands are:

* `add_from_live` - read the current value of an ODB key from the live experiment, and store it in any files that don't currently have that key. Files that already have that key present are not touched.
* `add` - like `add_from_live`, but manually specify the value to store as the `--json-value` parameter. You should encode the new value as the `value` member of a JSON string, e.g. `--json-value='{"value": true}'` to store a boolean true. Files that already have the key present are not touched. If you want to specify an explicit type (e.g. a uint16 rather than just a generic integer), you can pass the midas type ID as well, e.g. `--json-value='{"value": 3, "type": 4}'`. See `$MIDASSYS/python/midas/__init__.py` for the full list of midas type IDs.
* `delete` - remove the specified ODB key from all saved settings that contain it.
* `edit_from_live` - like `add_from_live`, but also change the value in files that already have the key present.
* `edit` - like `add`, but also change the value in files that already have the key present.

A full example command would be:

`python3.6 save_load.py add_from_live --odb-path="/Equipment/PPGCompiler/Settings/Enable formulae"`

## The web pages

To setup the web pages, add the following two keys in the ODB:

* `/Custom/saveload.js!` - set as the path to `saveload.js`; the `!` will mean that midas won't show a link in the menu.
* `/Custom/Save and load` - set as the path to `saveload.html`; midas will show a link in the menu on all webpages.

Usage of the web pages should be self-explanatory.
